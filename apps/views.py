from flask import Flask, request, session, redirect, url_for, render_template, flash, jsonify, redirect
from .models import Users, db, Posts, Rating
from .forms import SignUpForm, LogInForm, AddPostForm
from werkzeug.datastructures import CombinedMultiDict
from sqlalchemy.sql import func

from apps import app
from werkzeug.security import check_password_hash
from werkzeug.utils import secure_filename
from authlib.integrations.flask_client import OAuth
from apps.services import getThumbnailPresentation
import os
import re
from apps.upload_file import downloadImage, get_url_from_file_name, downloadGoogleImage, getAvatarUrl, uploadNewAvatar, getNewAvatarUrl
import datetime
from apps.__init__ import vers_config
import functools
import json

oauth = OAuth(app)
google = oauth.register(
    name='google',
    client_id=app.config['GOOGLE_CLIENT_ID'],
    client_secret=app.config['GOOGLE_CLIENT_SECRET'],
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    client_kwargs={'scope': 'openid email profile'},
)


# @app.before_request
# def before_request():
#     if not request.is_secure:
#         url = request.url.replace('http://', 'https://', 1)
#         code = 301
#         return redirect(url, code=code)


@app.route('/', methods=['GET'])
def index():
    if 'user_available' in session:
        q = request.args.get('q')
        # Show avatar
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            avatar = user.avatar_name
            show_avatar = getNewAvatarUrl(avatar)
        page = request.args.get('page', 1, type=int)
        search = ''
        if q:
            posts = Posts.query.filter(Posts.title.contains(q))
            search = '&q='+q
        else:
            posts = Posts.query.order_by(Posts.post_id.desc())

        # create preview url
        for post in posts:
            s3imageName = post.s3_image_name if post.s3_image_name is not None and post.s3_image_name != '' else post.url
            post.url = get_url_from_file_name(s3imageName)

        posts = posts.paginate(page=page, per_page=6)
        count_posts = len(posts.items)
        return render_template('index.html', posts=posts, avatar=show_avatar, search=search, count_post=count_posts, vers_config=vers_config)
    else:
        flash('You need to login first !')
        return redirect(url_for('login'))


@app.route('/login/google')
def google_login():
    google = oauth.create_client('google')
    redirect_uri = url_for('google_authorize', _external=True)
    return google.authorize_redirect(redirect_uri)


@app.route('/login/google/authorize')
def google_authorize():
    try:
        google = oauth.create_client('google')
        token = google.authorize_access_token()
        user_info = google.get("userinfo").json()
        u_email = user_info.get("email", "")
        family_name = user_info.get('family_name', "")
        given_name = user_info.get('given_name', "")
        get_domain = re.search("@[\D.]+", u_email)
        domain = get_domain.group()
        domains = ["@co-well.com.vn", "@co-well.vn", "@co-well.jp"]
        full_name = f"{family_name} {given_name}"
        if domain in domains:
            user = Users.query.filter_by(email=u_email).first()
            userId = user_info.get('id', "")
            avatar = user_info.get('picture', "")
            avatar_name = downloadGoogleImage(
                avatar, userId) if not user or not user.avatar_name else user.avatar_name
            if not user:
                reg = Users(user_info.get('name', ""), user_info.get('family_name', ""),
                            user_info.get('given_name', ""), u_email, '', avatar_name)
                db.session.add(reg)
                db.session.commit()
            else:
                user.family_name = user_info.get('family_name', "")
                user.given_name = user_info.get('given_name', "")
                user.avatar_name = avatar_name
                db.session.commit()
            if os.path.exists(avatar_name):
                os.remove(avatar_name)
            session['current_user'] = user_info.get('name', "")
            session['user_avatar'] = user_info.get('picture', "")
            show_avatar = session['user_avatar']
            session['user_available'] = True
            return redirect(url_for('index'))
        else:
            flash('Your email domain is not accepted. Please use the email domain provided by Co-well Asia Company!')
            return redirect(url_for('login'))
    except Exception as error:
        print('something wrong while doing login')
        print(error)
        return redirect(url_for('login'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    signupform = SignUpForm(request.form)
    if request.method == 'POST':
        u_email = signupform.email.data
        get_domain = re.search("@[\D.]+", u_email)
        domain = get_domain.group()
        domains = ["@co-well.com.vn", "@co-well.vn", "@co-well.jp"]
        if domain in domains:
            u_name = signupform.name.data
            test_pw = signupform.password.data
            if len(signupform.password.data) > 8 and re.match(r"^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$])[\w\d@#$%^&+=]{8,}$", test_pw):
                print(signupform.password.data)
                if signupform.password.data == signupform.password_2.data:
                    em = signupform.email.data
                    user = Users.query.filter_by(email=em).first()
                    if user:
                        flash('Email address already exists')
                    else:
                        reg = Users(
                            signupform.name.data, signupform.email.data, signupform.password.data)
                        db.session.add(reg)
                        db.session.commit()
                        flash('Sign up successfully! Log in now.')
                        return redirect(url_for('login'))
                else:
                    flash('Password must match!')
            else:
                flash('Use 8 or more characters and combinations of uppercase letters, lowercase letters, numbers, and any of the special characters: @#$%^&+=')
        else:
            flash('Your email domain is not accepted. Please use the email domain provided by Co-well Asia Company')
    return render_template('signup.html', signupform=signupform)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'user_available' in session:
        return redirect(url_for('index'))
    loginform = LogInForm(request.form)
    if request.method == 'POST':
        em = loginform.email.data
        log = Users.query.filter_by(email=em).first()
        if log and check_password_hash(log.password, loginform.password.data):
            session['current_user'] = log.name
            session['user_available'] = True
            session['user_avatar'] = '../static/img/user.png'
            return redirect(url_for('index'))
        else:
            flash('Wrong email or password!')
    return render_template('login.html', loginform=loginform)


@app.route('/add', methods=['GET', 'POST'])
def add_post():
    if session['user_available']:
        addpostform = AddPostForm(
            CombinedMultiDict((request.files, request.form)))
        # TODO: we should check for user_id instead of name
        name = session.get('current_user')
        users = Users.query.all()
        user = list(filter(lambda u: u.name == name, users))[0]
        addpostform.posted_by.data = user.family_name + ' ' + user.given_name
        addpostform.speaker.choices = list(
            map(lambda u: (u.id, u.name), users))
        # Show avatar
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            avatar = user.avatar_name
            show_avatar = getNewAvatarUrl(avatar)

        if request.method == 'POST':
            add_url = addpostform.add_url.data
            add_image = addpostform.image.data
            if add_url != '':
                if add_url.startswith("https://docs.google.com/presentation/d/"):
                    url_id = re.findall(r"(.*?)\/", add_url)
                    if len(url_id) > 0:
                        url_id = url_id[-1]
                    page_id = re.findall(r"(id)\.(.*)", add_url)
                    if len(page_id) > 0:
                        page_id = page_id[0][1]
                    image = getThumbnailPresentation(url_id, page_id)
                    image_name = downloadImage(image)
                    date = datetime.datetime.now()
                    date_save = date.strftime("%d/%m/%Y")
                    newpost = Posts(user.id, addpostform.title.data, addpostform.posted_by.data, addpostform.date.data,
                                    addpostform.description.data, addpostform.add_url.data, '', date_save, addpostform.speaker.data, img_url=add_image, s3_image_name=image_name)
                    db.session.add(newpost)
                    db.session.commit()
                    if os.path.exists(image_name):
                        os.remove(image_name)
                else:
                    flash('Please enter the correct link')
                    return redirect(url_for('add_post'))
                return redirect(url_for('index'))
        return render_template('new_post.html', addpostform=addpostform, avatar=show_avatar, users=users, vers_config=vers_config)
    else:
        flash('User is not Authenticated')


@app.route('/detail/<post_id>', methods=['GET'])
def see_more(post_id):
    param_data = {}
    if 'user_available' in session:
        # Show avatar
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            avatar = user.avatar_name
            show_avatar = getNewAvatarUrl(avatar)

        # display preview
        post = Posts.query.filter_by(post_id=post_id).first()
        url = post.add_url
        search_url = re.search("(\/d\/)(.*?)\/", url)
        if search_url:
            match_url = search_url.group()
            preview_url = "https://drive.google.com/file" + match_url + "preview"
        else:
            preview_url = 'https://drive.google.com/file/' + url
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        host = post.puid == user.id or user.email == 'tranhnb8920@co-well.com.vn'

        # Display image
        url = ''
        listUrl = []
        imgDisplay = []
        if post.img_url:
            getUrl = post.img_url.split(' ')
            for img in getUrl:
                if img.startswith("https://drive.google.com/file/d/"):
                    imgId = re.findall(r"\/d\/(.+)\/", img)
                    listUrl = 'https://drive.google.com/uc?export=view&id=' + \
                        ''.join(imgId)

        # rating query and calculate
        ratingData = Rating.query.filter_by(post_id=post.post_id).all()
        rating = (list(filter(lambda r: r.user_id ==
                              user.id, ratingData)) or [None])[0] if ratingData else 0
        average = 0.0
        count_rating = 0
        average = 0
        average_calc = Rating.query.with_entities(
            func.avg(Rating.rating_score)).filter(Rating.post_id == post.post_id).scalar()
        average = round(float(average_calc), 1) if average_calc else average
        count_rating = len(ratingData)

        ava = []
        for index, ava in enumerate(ratingData):
            ratingData[index].rater_avatar = getNewAvatarUrl(
                ava.cmt.avatar_name)
        param_data.update({
            'user': user,
            'post': post,
            'host': host,
            'avatar': show_avatar,
            'preview_url': preview_url,
            'imgDisplay': listUrl,
            'ratingData': ratingData,
            'rating': rating,
            'average': average,
            'count_rating': count_rating,
            'name': name,
            'vers_config': vers_config
        })
        return render_template('detail.html', **param_data)
    return redirect(url_for('login'))


@app.route('/detail/<post_id>', methods=['POST'])
def rating_and_comment(post_id):
    try:
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        post = Posts.query.filter_by(post_id=post_id).first()
        ratingData = Rating.query.filter_by(post_id=post.post_id).all()
    
        rating = (list(filter(lambda r: r.user_id ==
                                user.id, ratingData)) or [None])[0] if ratingData else 0
        now = datetime.datetime.now()
        data = request.get_json()
        if data:
            if rating and rating.id:
                rating.user_id = user.id
                rating.post_id = post.post_id
                rating.rating_score = int(data.get("stars", 0))
                rating.comment = str(data.get("text", ''))
                rating.datetime = now
                db.session.add(rating)
                db.session.commit()
            else:
                ratingValue = int(data.get("stars", 0))
                cmtValue = str(data.get("text", ''))
                ratingObject = Rating(
                    user_id=user.id, post_id=post.post_id, rating_score=ratingValue, comment=cmtValue, datetime=now)
                db.session.add(ratingObject)
                db.session.commit()
            return {'status': 0, 'message': 'Rate Successfully'}
    except Exception as error:
        print(str(error))
        return {'status': 1, 'message': 'Error!'}

@app.route('/rating/delete', methods=['POST'])
def delete_rate():
    try:
        name = session.get('current_user')
        data = request.get_json()
        user = Users.query.filter_by(name=name).first()
        user_rate = Rating.query.filter_by(user_id=user.id, id = data["id"]).first()
        db.session.delete(user_rate)
        db.session.commit()

        return {'status': 0, 'message': 'Delete Successfully'}
    except Exception as error:
        print(str(error))
        return {'status': 1, 'message': 'Delete Error!'}


@app.route('/edit/<post_id>', methods=['GET', 'POST'])
def edit_post(post_id):
    addpostform = AddPostForm(CombinedMultiDict((request.files, request.form)))
    if session['user_available']:
        post = Posts.query.filter_by(post_id=post_id).first()
        # TODO: we should change it to use user_id instead of name.
        name = session.get('current_user')
        users = Users.query.all()
        user = list(filter(lambda u: u.name == name, users))[0]
        # Show avatar
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            avatar = user.avatar_name
            show_avatar = getNewAvatarUrl(avatar)

        if post.puid != user.id and user.email != 'tranhnb8920@co-well.com.vn':
            return redirect(url_for('index'))
        if request.method == 'POST':
            add_url = addpostform.add_url.data
            add_image = addpostform.image.data
            if add_url != '':
                if add_url.startswith("https://docs.google.com/presentation/d/"):
                    url_id = re.findall(r"(.*?)\/", add_url)
                    if len(url_id) > 0:
                        url_id = url_id[-1]
                    page_id = re.findall(r"(id)\.(.*)", add_url)
                    if len(page_id) > 0:
                        page_id = page_id[0][1]
                    image = getThumbnailPresentation(url_id, page_id)
                    image_name = downloadImage(image)
                    post.title = addpostform.title.data
                    # format date to DD/MM/YYYY
                    post.talkdate = addpostform.date.data
                    post.description = addpostform.description.data
                    post.add_url = addpostform.add_url.data
                    post.s3_image_name = image_name
                    post.speaker_id = addpostform.speaker.data
                    post.img_url = addpostform.image.data
                    db.session.add(post)
                    db.session.commit()
                    if os.path.exists(image_name):
                        os.remove(image_name)
                else:
                    flash('Please enter the correct link')
                    return redirect(url_for('add_post'))
                return redirect(url_for('index'))

            flash("Presentation Has Been Updated!")
            return redirect(url_for('see_more', post_id=post.post_id))

        addpostform.speaker.choices = list(
            map(lambda u: (u.id, u.name), users))
        addpostform.speaker.coerce = int
        addpostform.speaker.data = post.speaker_id
        addpostform.title.data = post.title
        addpostform.posted_by.data = post.posted_by
        addpostform.description.data = post.description
        addpostform.add_url.data = post.add_url
        if post.talkdate:
            try:
                addpostform.date.data = datetime.datetime.strptime(
                    post.talkdate, '%Y-%m-%d')
            except:
                print('date format error!')
        if post.img_url != '':
            addpostform.image.data = post.img_url
    return render_template('updating.html', addpostform=addpostform, avatar=show_avatar, vers_config=vers_config, users=users)


@app.route('/delete/<post_id>')
def delete_post(post_id):
    name = session.get('current_user')
    user = Users.query.filter_by(name=name).first()

    Rating.query.filter_by(post_id=post_id).delete()
    Posts.query.filter_by(post_id=post_id, puid=user.id).delete()
    db.session.commit()

    flash('Presentation has been deleted successfully')
    return redirect(url_for('index'))


@app.route('/presentations')
def mypost():
    name = session.get('current_user')
    user = Users.query.filter_by(name=name).first()
    posts = Posts.query.filter_by(speaker_id=user.id)
    # Show avatar
    if user.avatar_name.startswith("avatar"):
        show_avatar = session['user_avatar']
    else:
        avatar = user.avatar_name
        show_avatar = getNewAvatarUrl(avatar)
    for post in posts:
        s3imageName = post.s3_image_name if post.s3_image_name is not None and post.s3_image_name != '' else post.url
        post.url = get_url_from_file_name(s3imageName)
    return render_template('presentations.html', posts=posts, avatar=show_avatar, vers_config=vers_config)


@app.route('/profile', methods=['GET', 'POST'])
def profile():
    profile = SignUpForm(CombinedMultiDict((request.files, request.form)))
    if session['user_available']:
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        posts = Posts.query.filter_by(speaker_id=user.id).all()
        # Show avatar
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            avatar = user.avatar_name
            show_avatar = getNewAvatarUrl(avatar)
        # Rating of each post
        ratingData = Rating.query.filter_by(post_id=Posts.post_id).all()
        count_rate = Rating.query.with_entities(Rating.post_id, func.count(
            Rating.post_id)).group_by(Rating.post_id).all()
        rates = [dict(zip(['post_id', 'rate'], sublst))
                 for sublst in count_rate]

        # Average rating of ech post
        average = 0
        average_calc = Rating.query.with_entities(Rating.post_id, func.avg(
            Rating.rating_score)).group_by(Rating.post_id).all()
        avgs = [dict(zip(['post_id', 'avg_rate'], sub))
                for sub in average_calc]

        postResponse = [{
            'id': p.post_id,
            'title': p.title,
            'talkdate': p.talkdate,
            'avg_rate': round(float((list(filter(lambda avg: avg['post_id'] == p.post_id, avgs)) or [{'avg_rate': 0.0}])[0]['avg_rate']), 1),
            'rate_count': (list(filter(lambda r: r['post_id'] == p.post_id, rates)) or [{'rate': 0}])[0]['rate']
        }
            for p in posts]

        # Overall rate
        starCount = 0
        rateCount = 0
        for r in ratingData:
            for p in posts:
                if r.post_id == p.post_id:
                    starCount += r.rating_score
                    rateCount += 1

        totalPost = len(posts)
        ratingPosts = len(ratingData)
        avgStarCount = 0
        for r in postResponse:
            avgStarCount += r['avg_rate']
        lenofOverallRating = len(
            list(filter(lambda r: r['avg_rate'], postResponse)))
        if lenofOverallRating > 0:
            overallRatingAvg = round(
                float(avgStarCount / lenofOverallRating), 1)
        else:
            overallRatingAvg = 0

        profile.fullname = user.family_name + ' ' + user.given_name
        profile.name.data = user.name
        profile.email.data = user.email
        return render_template('profile.html', profile=profile, avatar=show_avatar, email=user.email, posts=postResponse, count_rating=rates, totalPost=totalPost, starCount=starCount, rateCount=rateCount, overallRatingAvg=overallRatingAvg, vers_config=vers_config)
    else:
        return redirect(url_for('login'))


@app.route('/profile/<speaker_id>', methods=['GET'])
def users_profile(speaker_id):
    if session['user_available']:
        # Show avatar
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        if user.avatar_name.startswith("avatar"):
            show_avatar = session['user_avatar']
        else:
            userAvatar = user.avatar_name
            show_avatar = getNewAvatarUrl(userAvatar)
        name = session.get('current_user')
        avatar = ''
        posts = Posts.query.filter_by(speaker_id=speaker_id).all()
        for auth in posts:
            if auth.speaker.family_name:
                author = auth.speaker.family_name + ' ' + auth.speaker.given_name
            else:
                author = auth.speaker.name
            email = Users.query.filter_by(id=auth.speaker_id).first().email

        # Rating of all posts
        ratingData = Rating.query.filter_by(post_id=Rating.post_id).all()

        starCount = 0
        rateCount = 0
        for r in ratingData:
            for p in posts:
                # User avatar
                if p.speaker_id == p.speaker.id:
                    if p.speaker.avatar_name is not None and p.speaker.avatar_name != '':
                        s3Avatar = p.speaker.avatar_name
                        avatar = getAvatarUrl(s3Avatar)
                    else:
                        avatar
                if r.post_id == p.post_id:
                    starCount += r.rating_score
                    rateCount += 1

        # Rating data of all posts created by puid
        count_rate = Rating.query.with_entities(Rating.post_id, func.count(
            Rating.post_id)).group_by(Rating.post_id).all()
        rates = [dict(zip(['post_id', 'rate'], sublst))
                 for sublst in count_rate]

        # Average rating of each post
        average = 0
        average_calc = Rating.query.with_entities(Rating.post_id, func.avg(
            Rating.rating_score)).group_by(Rating.post_id).all()
        avgs = [dict(zip(['post_id', 'avg_rate'], sub))
                for sub in average_calc]
        totalPost = len(posts)
        ratingPosts = len(ratingData)
        postResponse = [{
            'id': p.post_id,
            'title': p.title,
            'author': p.speaker_id,
            'talkdate': p.talkdate,
            'avg_rate': round(float((list(filter(lambda avg: avg['post_id'] == p.post_id, avgs)) or [{'avg_rate': 0.0}])[0]['avg_rate']), 1),
            'rate_count': (list(filter(lambda r: r['post_id'] == p.post_id, rates)) or [{'rate': 0}])[0]['rate']
        }
            for p in posts]

        avgStarCount = 0
        for r in postResponse:
            avgStarCount += r['avg_rate']

        lenofOverallRating = len(
            list(filter(lambda r: r['avg_rate'], postResponse)))
        if lenofOverallRating > 0:
            overallRatingAvg = round(
                float(avgStarCount / lenofOverallRating), 1)
        else:
            overallRatingAvg = 0

        return render_template('users_profile.html', user_avatar=avatar, speaker_id=speaker_id, avatar=show_avatar, posts=postResponse, count_rating=rates, author=author, email=email, totalPost=totalPost, starCount=starCount, rateCount=rateCount, overallRatingAvg=overallRatingAvg, vers_config=vers_config)
    else:
        return redirect(url_for('login'))


@app.route('/upload_avatar', methods=['POST'])
def upload_avatar():
    try:
        name = session.get('current_user')
        user = Users.query.filter_by(name=name).first()
        if request.method == 'POST':
            avatar = request.files['file']
            avatarName = "user_avatar_" + avatar.filename
            user.avatar_name = avatarName
            db.session.commit()
            uploadAvatar = uploadNewAvatar(avatar, avatarName)
            getUrl = getNewAvatarUrl(avatarName)
            print(getUrl)
        return {'status': 0, 'url': getUrl, 'message': 'Uploaded successfully'}
    except Exception as error:
        return {'status': 1, 'message': 'Error uploaded!'}


@app.route('/ranking')
def ranking():
    # Show avatar
    name = session.get('current_user')
    user = Users.query.filter_by(name=name).first()
    if user.avatar_name.startswith("avatar"):
        show_avatar = session['user_avatar']
    else:
        avatar = user.avatar_name
        show_avatar = getNewAvatarUrl(avatar)
    # Total posts, rates and voters
    posts = Posts.query.filter_by(speaker_id=Posts.speaker_id).all()
    totalTalks = len(posts)

    # Table query data
    talkdates = Posts.query.filter_by(talkdate=Posts.talkdate).all()
    speakers = Posts.query.join(Posts.speaker).with_entities(
        Users.id, Users.name, (func.count(Users.id))).group_by(Users.id).all()
    rates = Rating.query.join(Rating.ratingPost).with_entities(
        Posts.speaker_id, func.count(Rating.id), func.sum(Rating.rating_score) if Rating.rating_score else 0).group_by(Posts.speaker_id).all()

    newSpeakers = []
    for speaker in speakers:
        sData = [0, [speaker[0], speaker[1]], speaker[2], 0, 0, 0.0]
        for r in rates:
            if speaker.id == r[0] and r[2] > 0:
                sData[3] = str(r[1])
                sData[4] = str(r[2])
                sData[5] = str(round(float(r[2] / r[1]), 1))
                break
        newSpeakers.append(sData)

    totalSpeakers = len(speakers)
    countRating = Rating.query.with_entities(Rating.user_id, func.count(
        Rating.user_id)).group_by(Rating.user_id).all()
    totalVoters = len(countRating)
    return render_template('ranking.html', avatar=show_avatar, posts=posts, speakers=newSpeakers, countRating=countRating, totalTalks=totalTalks, totalSpeakers=totalSpeakers, totalVoters=totalVoters, vers_config=vers_config)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run()
