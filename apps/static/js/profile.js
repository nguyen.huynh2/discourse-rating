
$(document).ready(function () {
    handleClickAvatar();
    onChangeAvatar();
});

function handleClickAvatar() {
    $("input[type='image']").click(function () {
        $("input[id='my_file']").click();
    });
}

function onChangeAvatar() {
    $("input[id='my_file']").change(function () {
        var image_file = $("#my_file").prop("files")[0];

        console.log(image_file.name)
        var form_data = new FormData();
        form_data.append("file", image_file);
        $.ajax({
            type: 'POST',
            url: '/upload_avatar',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json'
        }).done(function (data, textStatus, jqXHR) {
            $("input[type='image']").attr("src", data.url);
            $("img[type='image']").attr("src", data.url);
        }).fail(function (data) {
            alert('error!');
        });
    });

}
