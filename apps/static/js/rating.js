
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

$(document).ready(function () {

    loadDefaultRating();

    $(".star").click(function () {
      updateStarValue(this);
    });

    $(".btn-show-delete").on("click", function () {
      $("#modal-delete").modal("show");
      console.log(this.id);
      $("#btn-delete-rate").attr("data-id", this.id);
    });

    $("#btn-delete-rate").on("click", function () {
      deleteRate($(this).attr("data-id"));
    });
});

function loadDefaultRating() {
  let ratingValue = $("#rating_score").val();
  if (ratingValue != 0) {
    $(`#star${ratingValue}`).prop('checked', true);
  }
}

function updateStarValue(star) {
  $('#rate-star').val($(star).val());
}

function ratingClick(star) {
    var rating = {
      stars: parseInt($('#rate-star').val()),
      text: $('#message-text').val()
    };
  
    var postId = parseInt($("input[name='post_id']").val());
    $.ajax({
      type: "POST",
      url: `/detail/${postId}`,
      contentType: 'application/json',
      data : JSON.stringify(rating)
    }).done(function (data, textStatus, jqXHR) {
    if (data.status == 0) {
      window.location.reload();
    }
  });
}

function deleteRate(id) {
  $.ajax({
    type: "POST",
    url: `/rating/delete`,
    contentType: 'application/json',
    data : JSON.stringify({id})
  }).done(function (data, textStatus, jqXHR) {
    if (data.status == 0) {
      $("#modal-delete").modal("hide");
      window.location.reload();
    }
  });
}