from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.orm import relationship
from apps import app, db


class Users(db.Model):
    id = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(40), nullable=False)
    family_name = db.Column(db.String(40), nullable=False)
    given_name = db.Column(db.String(40), nullable=False)
    email = db.Column(db.String(40), nullable=False)
    password = db.Column(db.String(200), nullable=False)
    avatar_name = db.Column(db.String(100))


    def __init__(self, name, family_name, given_name, email, password, avatar_name):
        self.name = name
        self.family_name = family_name
        self.given_name = given_name
        self.email = email
        self.password = generate_password_hash(password)
        self.avatar_name = avatar_name

    def getname(self):
        return self.name


class Posts(db.Model):
    post_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(100))
    posted_by = db.Column(db.String(100), nullable=False)
    talkdate = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(1000))
    add_url = db.Column(db.String(1000))  # presentation url
    url = db.Column(db.String(1000))  # preview image url
    created_day = db.Column(db.String(100), nullable=False)
    img_url = db.Column(db.String(1000))
    s3_image_name = db.Column(db.String(100))
    
    # Person who upload the file - created by
    puid = db.Column(db.Integer, db.ForeignKey(
        'users.user_id', ondelete='CASCADE'))
    uploader = relationship("Users", foreign_keys=[puid])

    # The speaker id
    speaker_id = db.Column(db.Integer, db.ForeignKey(
        'users.user_id', ondelete='CASCADE'))
    speaker = relationship("Users", foreign_keys=[speaker_id])

    
    def __init__(self, puid, title, posted_by, talkdate, description, add_url, url, created_day, speaker_id, img_url, s3_image_name=''):
        self.puid = puid
        self.title = title
        self.posted_by = posted_by
        self.talkdate = talkdate
        self.description = description
        self.url = url
        self.add_url = add_url
        self.created_day = created_day
        self.speaker_id = speaker_id
        self.img_url = img_url
        self.s3_image_name = s3_image_name


class Rating(db.Model):
    id = db.Column('rating_id', db.Integer,
                   primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey(
        'users.user_id', ondelete='CASCADE'), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey(
        'posts.post_id', ondelete='CASCADE'), nullable=False)
    ratingPost = relationship("Posts", foreign_keys=[post_id])
    rating_score = db.Column(db.Integer, nullable=False)
    
    # Add new comment
    comment = db.Column(db.String(1000), nullable=False)
    datetime = db.Column(db.DateTime, nullable=False)
    
    cmt = relationship("Users", foreign_keys=[user_id], overlaps="comments,user")

    def __init__(self, user_id, post_id, rating_score, comment, datetime):
        self.user_id = user_id  # Id of the rater
        self.post_id = post_id
        self.rating_score = rating_score
        self.comment = comment
        self.datetime = datetime    