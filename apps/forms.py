from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, SubmitField, PasswordField, BooleanField, ValidationError, SelectField
from flask_wtf.file import FileField, FileRequired,FileAllowed
from wtforms.fields.html5 import EmailField, DateField
from wtforms.validators import DataRequired, Length, Email


class SignUpForm(FlaskForm):
    name = TextField('Name', validators= [ DataRequired(), Length(min=2)])
    email = EmailField('Email', validators= [ DataRequired(), Length(min=6)])
    password = PasswordField('Password', validators=[ DataRequired(), Length(min=8)])
    password_2 = PasswordField('Confirm Password')
    submit = SubmitField('Sign Up')


class LogInForm(FlaskForm):
    email = EmailField('Email', validators = [DataRequired(), Email()])
    password = PasswordField('Password', validators = [DataRequired(), Length(min=8)])
    submit = SubmitField('Sign In')


class AddPostForm(FlaskForm):
    title = TextField('Title')
    date = DateField('Date of the Talk', format='%Y-%m-%d', validators = [DataRequired(), ])
    author = TextField('Author')
    posted_by = TextField('Posted by')
    description = TextAreaField('Description')
    add_url = TextAreaField('Add embed URL', validators=[FileRequired()])
    submit = SubmitField('Submit')
    speaker = SelectField('Speaker')
    image = TextField('Image')