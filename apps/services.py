from __future__ import print_function

from googleapiclient import discovery
from httplib2 import Http
from oauth2client import file, client, tools
from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/presentations.readonly']


def getThumbnailPresentation(presentationId, pageObjectId):
    try:
        credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', SCOPES)
        service = discovery.build('slides', 'v1', credentials=credentials)
        image = service.presentations().pages().getThumbnail(
            presentationId=presentationId, 
            pageObjectId=pageObjectId, # 'p1'
            thumbnailProperties_thumbnailSize='LARGE' # Modified
            ).execute()
        return image["contentUrl"]
    except Exception as e:
        print(e)
