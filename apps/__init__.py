import os
import pathlib
from flask import Flask
from authlib.integrations.flask_client import OAuth
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv


load_dotenv()


app = Flask(__name__)
env_config = os.getenv("APP_SETTINGS", "config.DevelopmentConfig")
vers_config = os.getenv("VERSION_INFO", "version information")

app.config.from_object(env_config)
app.secret_key = app.config.get("SECRET_KEY")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['GOOGLE_CLIENT_SECRET'] = app.config.get("GOOGLE_CLIENT_SECRET")
app.config['GOOGLE_CLIENT_ID'] = app.config.get("GOOGLE_CLIENT_ID")
uri = os.getenv("DATABASE_URL")
if uri.startswith("postgres://"):
    uri = uri.replace("postgres://", "postgresql://", 1)
print(uri)
app.config['SQLALCHEMY_DATABASE_URI'] = uri

app.config['AWS_SECRET_ACCESS_KEY'] = app.config.get("AWS_SECRET_ACCESS_KEY")
app.config['AWS_ACCESS_KEY'] = app.config.get("AWS_ACCESS_KEY")
app.config["AWS_S3_BUCKET"] = app.config.get("AWS_S3_BUCKET")

db = SQLAlchemy(app)
migrate = Migrate(app, db)

oauth = OAuth(app)

db.init_app(app)
migrate.init_app(app, db)

from apps import views
from apps import models