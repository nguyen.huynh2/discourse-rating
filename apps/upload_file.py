from __future__ import print_function
from apps import app
from datetime import datetime
import urllib.request
import boto3


DEFAULT_PRESENTATION_IMAGE = 'presentation_default_image.png'
DEFAULT_AVATAR_IMAGE = 'default.jpg'


def downloadImage(image):
    try:
        now = datetime.now().time()
        get_time = now.strftime("%Y%m%d%H_%M_%S")
        image_name = "cover_"+get_time+".jpg"
        data = urllib.request.urlretrieve(image, image_name)
        # Let's use Amazon S3
        s3 = boto3.resource('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        s3client = boto3.client('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        # Upload a new file
        open_file = open(image_name, 'rb')
        res = s3.Bucket(app.config.get("AWS_S3_BUCKET")).put_object(
            Key=image_name, Body=open_file)
        return image_name
    except Exception as error:
        return DEFAULT_PRESENTATION_IMAGE


def get_url_from_file_name(file_name):
    file_name = DEFAULT_PRESENTATION_IMAGE if file_name is None or file_name == '' else file_name

    s3client = boto3.client('s3', aws_access_key_id=app.config.get(
        "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))

    return s3client.generate_presigned_url('get_object', Params={'Bucket': app.config.get("AWS_S3_BUCKET"), 'Key': file_name}, ExpiresIn=1800)


def downloadGoogleImage(avatar, userId):
    try:
        avatar_name = "avatar_"+userId+".jpg"
        data = urllib.request.urlretrieve(avatar, avatar_name)
        # Let's use Amazon S3
        s3 = boto3.resource('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        s3client = boto3.client('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        # Upload a new file
        open_file = open(avatar_name, 'rb')
        res = s3.Bucket(app.config.get("AWS_S3_BUCKET")).put_object(
            Key=avatar_name, Body=open_file)
        return avatar_name
    except Exception as error:
        return ''


def getAvatarUrl(file_name):
    file_name = DEFAULT_AVATAR_IMAGE if file_name is None or file_name == '' else file_name

    s3client = boto3.client('s3', aws_access_key_id=app.config.get(
        "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))

    return s3client.generate_presigned_url('get_object', Params={'Bucket': app.config.get("AWS_S3_BUCKET"), 'Key': file_name}, ExpiresIn=1800)


def uploadNewAvatar(avatar, avatarName):
    try:
        # Let's use Amazon S3
        s3 = boto3.resource('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        s3client = boto3.client('s3', aws_access_key_id=app.config.get(
            "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))
        # Upload a new file
        res = s3.Bucket(app.config.get("AWS_S3_BUCKET")).put_object(
            Key=avatarName, Body=avatar.read())
        return avatarName
    except Exception as error:
        return print(error)


def getNewAvatarUrl(url):
    url = DEFAULT_AVATAR_IMAGE if url is None or url == '' else url

    s3client = boto3.client('s3', aws_access_key_id=app.config.get(
        "AWS_ACCESS_KEY"), aws_secret_access_key=app.config.get("AWS_SECRET_ACCESS_KEY"))

    return s3client.generate_presigned_url('get_object', Params={'Bucket': app.config.get("AWS_S3_BUCKET"), 'Key': url}, ExpiresIn=1800)
